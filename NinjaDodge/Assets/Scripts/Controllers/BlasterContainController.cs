using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlasterContainController : MonoBehaviour
{
    [SerializeField] Blaster[] blasters;

    public void StartShot()
    {
        foreach(var blaster in blasters)
        {
            blaster.Init();
        }
    }

    public void SetFrequence(int frequence)
    {
        foreach (var blaster in blasters)
        {
            blaster.frequenceRate = frequence;
        }
    }
}
