using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using DG.Tweening;
public class UIController : MonoBehaviour
{
    public static UIController Instance { get; private set; }
    [SerializeField] TMP_Text timeText;

    [Header("Heart")]
    [SerializeField] RectTransform heartContainRect;
    [SerializeField] Image[] hearts = new Image[3];
    [SerializeField] Sprite heartActive, heartInactive, starActive, starInactive;
    [SerializeField] GameObject[] vfx_starDusts;

    [Header("Anim element")]
    [SerializeField] RectTransform logoRect;
    [SerializeField] RectTransform tutorialRect;
    [SerializeField] RectTransform creditRect;
    [SerializeField] RectTransform remainTimeRect;
    [SerializeField] RectTransform timeUpRect;
    [SerializeField] Image blackFadeImage;
    [SerializeField] Color blackFadeColor;
    [SerializeField] TMP_Text curLevelText, endGameText1, endGameText2;

    [Header("Button element")]
    [SerializeField] RectTransform startBtnRect;
    [SerializeField] RectTransform levelBtnRect;
    [SerializeField] RectTransform backBtnRect;
    [SerializeField] RectTransform restartBtnRect;

    [SerializeField] LevelButton[] levelButtons;

    AudioSource audioSource;
    [Header("Sound effect")]
    [SerializeField] AudioClip getStarClip;
    [SerializeField] AudioClip time10sClip, timeUpClip, resultClip;

    public void Awake()
    {
        Instance = this;
        audioSource = GetComponent<AudioSource>();
    }
    public void SetRemainTime(int val)
    {
        timeText.text = "TIME: " + val.ToString();
        if (val <= 10)
        {
            audioSource.PlayOneShot(time10sClip);
            timeText.rectTransform.DOShakeScale(0.5f, 0.3f);
            StartCoroutine(Cor_Text10sAnim());
        }
    }

    IEnumerator Cor_Text10sAnim()
    {
        timeText.DOColor(Color.yellow, 0.25f);
        yield return new WaitForSeconds(0.25f);
        timeText.DOColor(Color.white, 0.25f);
    }

    public void SetHeart(int val)
    {
        if (val>=0 && val<3)
        {
            hearts[val].rectTransform.DOShakeScale(0.5f, 1);
        }
        for(int i=0; i<3; i++)
        {
            if (i < val)
            {
                hearts[i].sprite = heartActive;
            }
            else
            {
                hearts[i].sprite = heartInactive;
            }
        }
    }
    public void EndGameAnim()
    {
        //timeUpRect.gameObject.SetActive(true);
        //blackFadeImage.gameObject.SetActive(true);
        if (PlayerMove.Instance.hp > 0)
        {
            audioSource.PlayOneShot(timeUpClip);
            endGameText1.text = endGameText2.text = "TIME UP!";
            timeUpRect.anchoredPosition = Vector3.zero;
        }
        else
        {
            endGameText1.text = endGameText2.text = "NEVER GIVE UP!";
        }
        blackFadeImage.rectTransform.anchoredPosition = Vector3.zero;
        blackFadeImage.color = Color.clear;
        timeUpRect.DOShakeScale(1, 1);
        UpdateStarLevelButton();
        
        StartCoroutine(Cor_EndGameAnim());
    }
    public void UpdateStarLevelButton()
    {
        foreach (var levelButton in levelButtons)
        {
            levelButton.SetStar();
        }
    }
    public void ToIntroAnim()
    {
        timeUpRect.DOAnchorPosY(1200, 1);
        blackFadeImage.DOColor(Color.clear, 1);
        heartContainRect.DOAnchorPosY(100, 1);
        remainTimeRect.DOAnchorPosY(100, 1);
        startBtnRect.DOAnchorPosY(-300, 1);
        levelBtnRect.DOAnchorPosY(-700, 1);
        logoRect.DOAnchorPosY(90, 1);
        backBtnRect.DOAnchorPosY(700, 1);
        tutorialRect.DOAnchorPosY(800, 1);
        creditRect.DOAnchorPosY(35, 1);
    }
    public void ToMenuAnim()
    {
        StopAllCoroutines();
        timeUpRect.DOAnchorPosY(1200, 1);
        blackFadeImage.DOColor(Color.clear, 1);
        heartContainRect.DOAnchorPosY(100, 1);
        remainTimeRect.DOAnchorPosY(100, 1);
        levelBtnRect.DOAnchorPosY(-350, 1);
        startBtnRect.DOAnchorPosY(-700, 1);
        logoRect.DOAnchorPosY(500, 1);
        backBtnRect.DOAnchorPosY(460, 1);
        tutorialRect.DOAnchorPosY(350, 1);
        creditRect.DOAnchorPosY(-35, 1);
        curLevelText.DOColor(Color.clear, 1);
        restartBtnRect.DOAnchorPosY(-700, 1);
    }
    public void ToGameplayAnim()
    {
        StopAllCoroutines();
        timeUpRect.DOAnchorPosY(1200, 1);
        blackFadeImage.DOColor(Color.clear, 1);
        heartContainRect.DOAnchorPos(new Vector3(175,-70,0), 1);
        heartContainRect.DOScale(1, 1);
        remainTimeRect.DOAnchorPosY(-50, 1);
        levelBtnRect.DOAnchorPosY(-700, 1);
        startBtnRect.DOAnchorPosY(-700, 1);
        backBtnRect.DOAnchorPosY(-460, 1);
        tutorialRect.DOAnchorPosY(800, 1);
        creditRect.DOAnchorPosY(-35, 1);
        curLevelText.DOColor(Color.clear, 1);
        restartBtnRect.DOAnchorPosY(-700, 1);
    }
    IEnumerator Cor_EndGameAnim()
    {
        yield return new WaitForSeconds(3);

        audioSource.PlayOneShot(resultClip);
        timeUpRect.DOAnchorPosY(300, 1);
        heartContainRect.DOAnchorPos(new Vector3(960, -560, 0), 1);
        heartContainRect.DOScale(1.5f, 1);
        blackFadeImage.DOColor(blackFadeColor, 1);
        restartBtnRect.DOAnchorPosY(-300, 1);
        curLevelText.text = "Level " + LevelController.Instance.curLevel.ToString();
        curLevelText.DOColor(Color.white, 1);
        yield return new WaitForSeconds(1);
        for (int i=0; i<3; i++)
        {
            yield return new WaitForSeconds(0.5f);
            if (i < PlayerMove.Instance.hp)
            {
                hearts[i].sprite = starActive;
                vfx_starDusts[i].SetActive(true);
                audioSource.clip = getStarClip;
                audioSource.Play();
            }
            else
                hearts[i].sprite = starInactive;
            hearts[i].rectTransform.DOShakeScale(0.5f, 1);
            StartCoroutine(Cor_ToNormalScale(hearts[i].rectTransform, 0.5f));
        }
    }
    IEnumerator Cor_ToNormalScale(RectTransform rect, float sec)
    {
        yield return new WaitForSeconds(sec);
        rect.DOScale(1, 0.1f);
    }
}
