using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelController : MonoBehaviour
{
    public static LevelController Instance { get; private set; }
    [SerializeField] BlasterContainController birdBlasterContainLeft, birdBlasterContainRight, 
        fireBlasterContainL, fireBlasterContainR, fireBlasterContainU, fireBlasterContainD;
    [SerializeField] ThinGroundController thinGroundController;

    public int curLevel;
    public int[] starLevels = new int[3];

    [Space(10)]
    [SerializeField] Transform lightTrans;
    [SerializeField] Light sunLight, moonLight;

    [Header("Day")]
    [SerializeField] Color dayBgColor;
    [SerializeField] Color dayColor;
    [SerializeField] float dayLightAngle;

    [Header("Sunset")]
    [SerializeField] Color sunsetBgColor;
    [SerializeField] Color sunsetColor;
    [SerializeField] float sunsetLightAngle;

    [Header("Night")]
    [SerializeField] Color nightBgColor;
    [SerializeField] float nightLightAngle;
    private void Awake()
    {
        Instance = this;
    }

    public void Level1()
    {
        Debug.Log("Level 1");
        curLevel = 1;

        birdBlasterContainLeft.gameObject.SetActive(true);
        birdBlasterContainLeft.StartShot();
        birdBlasterContainLeft.SetFrequence(80);
    }

    public void Level2()
    {
        Debug.Log("Level 2");
        curLevel = 2;

        thinGroundController.thinGrounds[1].Grow();
        birdBlasterContainLeft.gameObject.SetActive(true);
        birdBlasterContainLeft.SetFrequence(65);
        birdBlasterContainLeft.StartShot();
    }

    public void Level3()
    {
        Debug.Log("Level 3");
        curLevel = 3;
        fireBlasterContainL.gameObject.SetActive(true);
        fireBlasterContainL.StartShot();
    }

    public void ToCurLevel()
    {
        if (curLevel == 1)
        {
            Level1();
        }
        else if (curLevel == 2)
        {
            Level2();
        }
        else if (curLevel == 3)
        {
            Level3();
        }
    }    

    public void SetDifficultWithTime(int remainTime)
    {
        if (curLevel == 1)
        {
            if (remainTime == 40)
            {
                birdBlasterContainRight.gameObject.SetActive(true);
                birdBlasterContainRight.StartShot();
            }
            else if (remainTime == 20)
            {
                thinGroundController.thinGrounds[1].Grow();
            }
        }
        else if(curLevel == 2)
        {
            if (remainTime == 50)
            {
                birdBlasterContainLeft.SetFrequence(0);
            }
            else if (remainTime == 45)
            {
                thinGroundController.thinGrounds[0].Grow();
                birdBlasterContainLeft.gameObject.SetActive(false);
            }
            else if (remainTime == 30)
            {
                thinGroundController.thinGrounds[3].Grow();
            }
            else if (remainTime == 15)
            {
                thinGroundController.thinGrounds[2].Grow();
            }
        }
        else if(curLevel == 3)
        {
            if (remainTime == 50)
            {
                fireBlasterContainR.gameObject.SetActive(true);
                fireBlasterContainR.StartShot();
            }
            else if (remainTime == 35)
            {
                fireBlasterContainD.gameObject.SetActive(true);
                fireBlasterContainD.StartShot();
            }
            else if (remainTime == 20)
            {
                fireBlasterContainU.gameObject.SetActive(true);
                fireBlasterContainU.StartShot();
            }
        }
    }

    public void HideObject()
    {
        birdBlasterContainLeft.gameObject.SetActive(false);
        birdBlasterContainRight.gameObject.SetActive(false);
        fireBlasterContainD.gameObject.SetActive(false);
        fireBlasterContainU.gameObject.SetActive(false);
        fireBlasterContainL.gameObject.SetActive(false);
        fireBlasterContainR.gameObject.SetActive(false);
        thinGroundController.HideAll();
    }    

    public void ToDayTime()
    {
        StopAllCoroutines();
        sunLight.gameObject.SetActive(true);
        sunLight.DOIntensity(1, 1);
        sunLight.DOColor(dayColor, 1);
        if (moonLight.gameObject.activeSelf == true)
        {
            moonLight.DOIntensity(0, 1);
            StartCoroutine(Cor_SetActiveFalse(moonLight.gameObject, 1));
        }
        CameraController.Instance.GetComponent<Camera>().DOColor(dayBgColor, 1);
        lightTrans.DORotate(new Vector3(dayLightAngle, 30, 0), 1);
    }
    public void ToSunsetTime()
    {
        StopAllCoroutines();
        sunLight.gameObject.SetActive(true);
        sunLight.DOIntensity(2, 1);
        sunLight.DOColor(sunsetColor, 1);
        if (moonLight.gameObject.activeSelf == true)
        {
            moonLight.DOIntensity(0, 1);
            StartCoroutine(Cor_SetActiveFalse(moonLight.gameObject, 1));
        }
        CameraController.Instance.GetComponent<Camera>().DOColor(sunsetBgColor, 1);
        lightTrans.DORotate(new Vector3(sunsetLightAngle, 30, 0), 1);
    }
    public void ToNightTime()
    {
        StopAllCoroutines();
        moonLight.gameObject.SetActive(true);
        moonLight.DOIntensity(1, 1);
        //moonLight.DOColor(nightColor, 1);
        if (sunLight.gameObject.activeSelf == true)
        {
            sunLight.DOIntensity(0, 1);
            StartCoroutine(Cor_SetActiveFalse(sunLight.gameObject, 1));
        }
        CameraController.Instance.GetComponent<Camera>().DOColor(nightBgColor, 1);
        lightTrans.DORotate(new Vector3(nightLightAngle, 30, 0), 1);
    }
    IEnumerator Cor_SetActiveFalse(GameObject obj, float sec)
    {
        yield return new WaitForSeconds(sec);
        obj.SetActive(false);
    }
}
