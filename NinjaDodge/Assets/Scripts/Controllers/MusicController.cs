using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MusicController : MonoBehaviour
{
    public static MusicController Instance { get; private set; }
    AudioSource audioSource;
    [SerializeField] AudioClip menuClip, gameplayClip;

    bool isPlayMenuTheme;

    public bool isMusicOff;
    private void Awake()
    {
        Instance = this;
        audioSource = GetComponent<AudioSource>();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            if (isMusicOff == false)
            {
                isMusicOff = true;
                audioSource.volume = 0;
            }
            else
            {
                isMusicOff = false;
                audioSource.volume = 0.8f;
            }
        }
    }
    public void PlayMenuTheme()
    {
        if (isMusicOff) return;

        if (isPlayMenuTheme == true) return;
        StopAllCoroutines();
        audioSource.Stop();

        isPlayMenuTheme = true;
        audioSource.volume = 0.8f;
        audioSource.clip = menuClip;
        audioSource.Play();     

    }

    public void PlayGameplayThemeAfter(float sec)
    {
        LowDownMusic();
        isPlayMenuTheme = false;
        if (isMusicOff) return;

        StartCoroutine(Cor_PlayGameplay(sec));
    }

    IEnumerator Cor_PlayGameplay(float sec)
    {
        yield return new WaitForSeconds(sec);
        audioSource.volume = 0.8f;
        audioSource.clip = gameplayClip;
        audioSource.Play();
    }

    public void LowDownMusic()
    {
        if (isMusicOff) return;

        audioSource.DOFade(0, 0.8f);
    }
}
