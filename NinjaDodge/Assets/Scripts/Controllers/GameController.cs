using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public static GameController Instance { get; private set; }
    public int remainTime, timeSet;
    public int gameState = 0; //0: intro, 1: menu, 2: gameplay, 3: endgame

    [SerializeField] GameObject vfx_thunder;

    Coroutine timeCor;

    AudioSource audioSource;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        Instance = this;
    }

    private void Start()
    {
        StartCoroutine(Cor_ToIntro(2f));
        //LevelController.Instance.ToSunsetTime();
    }

    public void ToGameplay()
    {
        MusicController.Instance.PlayGameplayThemeAfter(1);
        gameState = 2;
        CameraController.Instance.ToGameplay();
        PlayerMove.Instance.SetBound(-10, 10, -10, 10);
        PlayerMove.Instance.EnableColliderIn2D(false);
        PlayerMove.Instance.hp = 3;
        UIController.Instance.SetHeart(3);
        UIController.Instance.ToGameplayAnim();

        remainTime = timeSet;
        UIController.Instance.SetRemainTime(remainTime);
        timeCor = StartCoroutine(Cor_ReduceTime());
    }
    public void ToMenu()
    {
        MusicController.Instance.PlayMenuTheme();
        gameState = 1;
        CameraController.Instance.ToMenu();
        LevelController.Instance.HideObject();
        PlayerMove.Instance.SetBound(-5, 5, 0, 0);
        PlayerMove.Instance.EnableColliderIn2D(true);
        if (PlayerMove.Instance.hp <= 0)
        {
            PlayerMove.Instance.hp = 3;
            PlayerMove.Instance.Respawn();
        }
        UIController.Instance.ToMenuAnim();

        if (timeCor != null)
            StopCoroutine(timeCor);
    }

    public void ToIntro()
    {
        MusicController.Instance.PlayMenuTheme();
        gameState = 0;
        CameraController.Instance.ToMenu();
        LevelController.Instance.HideObject();
        PlayerMove.Instance.SetBound(-5, 5, 0, 0);
        PlayerMove.Instance.EnableColliderIn2D(true);
        //PlayerMove.Instance.Respawn();
        UIController.Instance.ToIntroAnim();
    }

    IEnumerator Cor_ToIntro(float sec)
    {
        yield return new WaitUntil(() => CameraController.Instance != null);
        CameraController.Instance.transform.DOMoveY(0, 1.75f);

        yield return new WaitUntil(() => PlayerMove.Instance != null);
        PlayerMove.Instance.hp = 0;
        PlayerMove.Instance.rb.velocity = Vector3.zero;
        PlayerMove.Instance.rb.useGravity = false;
        yield return new WaitForSeconds(sec);
        audioSource.Play();
        PlayerMove.Instance.hp = 3;
        PlayerMove.Instance.rb.useGravity = true;
        yield return new WaitUntil(() => PlayerMove.Instance.transform.position.y <= 0.2f);
        CameraController.Instance.transform.DOMoveX(0.3f,0.07f).OnComplete(() =>
        {
            CameraController.Instance.transform.DOMoveX(-0.3f, 0.14f).OnComplete(() =>
            {
                CameraController.Instance.transform.DOMoveX(0f, 0.07f);
            });
        });

        vfx_thunder.SetActive(true);
        vfx_thunder.transform.position = new Vector3(PlayerMove.Instance.transform.position.x, 0, -4);
        ToIntro();
        yield return new WaitForSeconds(1);
        vfx_thunder.SetActive(false);
    }

    public void Back()
    {
        if (gameState == 1)
        {
            ToIntro();
        }
        else if (gameState > 1)
        {
            ToMenu();
        }
    }
    public void Restart()
    {
        ToGameplay();
        LevelController.Instance.ToCurLevel();
        PlayerMove.Instance.Respawn();
    }    
    public void EndGame()
    {
        gameState = 3;
        PlayerMove.Instance.SetBound(-5, 5, -5, 5);
        PlayerMove.Instance.Respawn();
        LevelController.Instance.HideObject();
        MusicController.Instance.LowDownMusic();

        if (timeCor != null)
            StopCoroutine(timeCor);

        LevelController.Instance.starLevels[LevelController.Instance.curLevel - 1] = PlayerMove.Instance.hp;
        UIController.Instance.EndGameAnim();
        Debug.Log("Endgame");
    }

    IEnumerator Cor_ReduceTime()
    {
        yield return new WaitForSeconds(1);
        if (gameState != 3)
        {
            remainTime--;
            UIController.Instance.SetRemainTime(remainTime);
            LevelController.Instance.SetDifficultWithTime(remainTime);
            if (remainTime > 0)
            {
                timeCor = StartCoroutine(Cor_ReduceTime());
            }
            else
            {
                EndGame();
            }
        }
    }
}
