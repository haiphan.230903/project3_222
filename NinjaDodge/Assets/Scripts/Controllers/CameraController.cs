using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public static CameraController Instance { get; private set; }
    static Animator anim;
    public float maxX = 0, minX = 0, maxY = 0, minY = 0, maxZ = 0, minZ = 0;
    public float moveSpeed;

    public bool isFollowPlayer, isOnMenu;
    //bool isScoll = false;

    Rigidbody2D rigi;

    Vector3 target;
    // Start is called before the first frame update
    void Awake()
    {
        Instance = this;
        //anim = GetComponent<Animator>();
        //rigi = GetComponent<Rigidbody2D>();
    }

    private void Start()
    {
        isFollowPlayer = false;
        isOnMenu = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (PlayerMove.Instance != null && isFollowPlayer)
        {
            // Tao camera di chuyen theo targetObj
            target = PlayerMove.Instance.transform.position;
            target.z -= 12;

            if (target.x < minX) target.x = minX;
            if (target.x >= maxX) target.x = maxX;
            if (target.y < minY) target.y = minY;
            if (target.y >= maxY) target.y = maxY;
            if (target.z < minZ) target.z = minZ;
            if (target.z >= maxZ) target.z = maxZ;            
            
            transform.position = Vector3.Lerp(transform.position, target, moveSpeed);
        }

        //if (isFollowPlayer == false) Invoke("FollowPlayer", 0.5f);
    }

    public Vector2 CameraPos
    {
        get { return transform.position; }
        set { 
                transform.position = value;
            transform.position -= new Vector3(0, 0, 10);
            }
    }

    public void ToGameplay()
    {
        isOnMenu = false;
        transform.DORotate(new Vector3(35, 0, 0),2);
        //transform.DOMoveX(0, 2);
        transform.DOMoveY(7, 2);
        transform.DOMoveZ(-12, 2);
        StartCoroutine(Cor_FollowPlayer(2.5f));
    }
    public void ToMenu()
    {
        isFollowPlayer = false;
        isOnMenu = true;
        transform.DORotate(new Vector3(0, 0, 0), 2);
        transform.DOMoveX(0, 2);
        transform.DOMoveY(0, 2);
        transform.DOMoveZ(-18, 2);
    }
    IEnumerator Cor_FollowPlayer(float sec)
    {
        yield return new WaitForSeconds(sec);
        if (isOnMenu == false)
        {
            isFollowPlayer = true;
        }
    }
}
