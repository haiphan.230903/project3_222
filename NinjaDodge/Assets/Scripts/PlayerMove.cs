using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PlayerMove : MonoBehaviour
{
    public static PlayerMove Instance { get; private set; }

    [SerializeField] BoxCollider In2DCollider;
    [SerializeField] float moveSpeed, dashSpeed, jumpForce, hurtPushForce, hurtTime, dashTime, recoverdashTime;
    public Animator graphicAnim;
    [SerializeField] SpriteRenderer playerSpr;
    [SerializeField] SpriteRenderer[] ninjaAfterImages;
    public Rigidbody rb;
    [SerializeField] GameObject vfx_dust, vfx_jump;
    float xDir, zDir, xDirRaw, zDirRaw, xDirCur, zDirCur, xDash, zDash; //xDirAnim va zDirAnim dung de giu huong quay hien tai khi khong di chuyen
    float minX, maxX,/* minY, maxY,*/ minZ, maxZ, dashRemainTime;
    public int hp;
    bool isMoving, isJumping = true, isHurting, isDashing, isCanDash = true, isFirstIntro = true;

    AudioSource audioSource;
    [Header("Sound effect")]
    [SerializeField] AudioClip jumpClip;
    [SerializeField] AudioClip dashClip, landClip, hurtClip, dieClip;


    private void Awake()
    {
        Instance = this;
        audioSource = GetComponent<AudioSource>();
    }
    // Update is called once per frame
    void Update()
    {
        if (hp<=0) return;

        xDir = Input.GetAxis("Horizontal");
        zDir = Input.GetAxis("Vertical");
        xDirRaw = Input.GetAxisRaw("Horizontal");
        zDirRaw = Input.GetAxisRaw("Vertical");

        if (Input.GetKeyDown(KeyCode.M))
        {
            Jump();
        }
        if (Input.GetKeyDown(KeyCode.N))
        {
            Dash();
        }

        if (xDir != 0 || zDir != 0)
        {
            isMoving = true;
            if (xDirRaw != 0 || zDirRaw != 0)
            {
                xDirCur = xDir;
                zDirCur = zDir;
            }
        }
        else
            isMoving = false;

        graphicAnim.SetFloat("xDir", xDirCur);
        graphicAnim.SetFloat("zDir", zDirCur);
        graphicAnim.SetBool("isMoving", isMoving);
        graphicAnim.SetBool("isJumping", isJumping);

        if (isMoving == true && isDashing == false)
            Move();

        Bound();
    }

    void Bound()
    {
        if (transform.position.x > maxX) transform.position = new Vector3(maxX, transform.position.y, transform.position.z);
        if (transform.position.x < minX) transform.position = new Vector3(minX, transform.position.y, transform.position.z);
        //if (transform.position.y > maxY) transform.position = new Vector3(transform.position.x, minY, transform.position.z);
        //if (transform.position.y < minY) transform.position = new Vector3(transform.position.x, maxY, transform.position.z);
        if (transform.position.z > maxZ) transform.position = new Vector3(transform.position.x, transform.position.y, maxZ);
        if (transform.position.z < minZ) transform.position = new Vector3(transform.position.x, transform.position.y, minZ);
    }

    public void SetBound(float minX, float maxX/*, float minY, float maxY*/, float minZ, float maxZ)
    {
        this.minX = minX;
        this.maxX = maxX;
        //this.minY = minY;
        //this.maxY = maxY;
        this.minZ = minZ;
        this.maxZ = maxZ;
    }

    void Move()
    {
        Vector3 dir = new Vector3(xDir, 0, zDir);
        if (xDir * xDir + zDir * zDir > 1)
            dir = dir.normalized;
        transform.Translate(dir * moveSpeed * Time.deltaTime);
    }
    void Dash()
    {
        if (isCanDash == false) return;

        audioSource.clip = dashClip;
        audioSource.Play();
        isDashing = true;
        isCanDash = false;
        xDash = xDirCur;
        zDash = zDirCur;
        dashRemainTime = dashTime;
        StartCoroutine(Cor_Dash());
        StartCoroutine(Cor_RecoverDash());
        StartCoroutine(Cor_AfterImage());
    }
    void Jump()
    {
        if (isJumping == true) return;

        audioSource.clip = jumpClip;
        audioSource.Play();

        vfx_jump.SetActive(true);

        graphicAnim.transform.DOScale(new Vector3(0.85f, 1.2f, 1), 0.5f);
        //Debug.Log("Jump");
        isJumping = true;
        rb.AddForce(Vector3.up * jumpForce);
    }

    void Hurt()
    {
        if (hp <= 0) return;
        audioSource.clip = hurtClip;
        audioSource.Play();
        hp--;
        CameraController.Instance.transform.DOShakePosition(0.5f, 0.7f);
        UIController.Instance.SetHeart(hp);
        if (hp <= 0)
        {
            GameController.Instance.gameState = 3;
            Invoke("Die", 0.5f);
            return;
        }

        isHurting = true;
        isDashing = false;
        graphicAnim.SetBool("isHurting", true);
        StartCoroutine(Cor_EndHurt());
        StartCoroutine(Cor_HurtAnim());
    }

    void Die()
    {
        GameController.Instance.EndGame();
        graphicAnim.SetBool("isDead", true);
        graphicAnim.Play("Die");
        audioSource.clip = dieClip;
        audioSource.Play();
        StartCoroutine(Cor_DieColapseAnim());
    }

    public void Respawn()
    {
        graphicAnim.SetBool("isDead", false);
        Debug.Log("Respawn");
        rb.velocity = Vector3.zero;
        transform.position = Vector3.zero;
    }

    public void EnableColliderIn2D(bool is2D)
    {
        if (is2D)
            In2DCollider.enabled = true;
        else
            In2DCollider.enabled = false;
    }

    IEnumerator Cor_Dash()
    {
        while (isDashing == true)
        {
            transform.Translate(new Vector3(xDash, 0, zDash).normalized * dashSpeed * Time.deltaTime);
            dashRemainTime -= 0.02f;
            yield return (0.02f);
            if (dashRemainTime <= 0)
            {
                isDashing = false;
            }
        }
    }
    IEnumerator Cor_RecoverDash()
    {
        yield return new WaitForSeconds(recoverdashTime);
        isCanDash = true;
    }
    IEnumerator Cor_AfterImage()
    {
        foreach(var afterImage in ninjaAfterImages)
        {
            DOTween.Kill(afterImage);
            afterImage.gameObject.SetActive(true);
            afterImage.transform.position = transform.position;
            afterImage.sprite = playerSpr.sprite;
            Color color = Color.white;
            color.a = 0.7f;
            afterImage.color = color;
            color.a = 0f;
            afterImage.DOColor(color, 0.3f);
            yield return new WaitForSeconds(0.06f);
        }
        yield return new WaitForSeconds(0.5f);
        foreach (var afterImage in ninjaAfterImages)
        {
            afterImage.gameObject.SetActive(false);
        }
    }
    IEnumerator Cor_DieColapseAnim()
    {
        yield return new WaitForSeconds(2.25f);
        vfx_dust.SetActive(true);
        graphicAnim.Play("colapse");
        audioSource.clip = hurtClip;
        audioSource.Play();
        graphicAnim.transform.DOScale(new Vector3(1.2f, 0.8f, 1), 0.15f);
        yield return new WaitForSeconds(0.15f);
        graphicAnim.transform.DOScale(Vector3.one, 0.15f);
        yield return new WaitForSeconds(1);
        
    }
    IEnumerator Cor_EndHurt()
    {
        yield return new WaitForSeconds(hurtTime);
        isHurting = false;
    }
    IEnumerator Cor_HurtAnim()
    {
        playerSpr.DOColor(Color.red, 0.2f);
        yield return new WaitForSeconds(0.2f);
        playerSpr.DOColor(Color.white, 0.2f);
        yield return new WaitForSeconds(0.2f);
        graphicAnim.SetBool("isHurting", false); //Hurting trong anim khac voi hurting trong PlayerMove, trong PlayerMove bao gom ca trang thai blinking
        StartCoroutine(Cor_BlinkAnim());
    }
    IEnumerator Cor_BlinkAnim()
    {
        while (isHurting == true)
        {
            playerSpr.DOColor(new Color(1, 1, 1, 0.3f), 0.2f);
            yield return new WaitForSeconds(0.2f);
            playerSpr.DOColor(new Color(1, 1, 1, 1f), 0.2f);
            yield return new WaitForSeconds(0.2f);
        }
    }
    IEnumerator Cor_ToNormalScale(float sec)
    {
        yield return new WaitForSeconds(sec);
        graphicAnim.transform.DOScale(Vector3.one, 0.15f);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            isJumping = false;
            if (hp > 0)
            {
                audioSource.clip = landClip;
                audioSource.Play();
            }
            vfx_dust.SetActive(true);
            graphicAnim.transform.DOScale(new Vector3(1.2f, 0.8f, 1), 0.15f);
            StartCoroutine(Cor_ToNormalScale(0.15f));
        }
        else if (collision.gameObject.CompareTag("Enemy"))
        {
            if (isHurting == false)
            {
                rb.AddForce(((transform.position - collision.transform.position) + Vector3.up).normalized * hurtPushForce);
                Hurt();
            }
        }
        else if (collision.gameObject.CompareTag("Finish"))
        {
            hp = 0;
            UIController.Instance.SetHeart(2);
            UIController.Instance.SetHeart(1);
            UIController.Instance.SetHeart(0);
            CameraController.Instance.transform.DOShakePosition(0.5f, 0.7f);
            Die();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Enemy"))
        {
            if (isHurting == false)
            {
                rb.AddForce(((transform.position - other.transform.position) + Vector3.up).normalized * hurtPushForce);
                Hurt();
            }
        }
    }
}
