using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Grass : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        transform.DOScaleX(1.3f, 0.25f);
        transform.DOScaleY(0.7f, 0.25f);
    }

    private void OnTriggerExit(Collider other)
    {
        transform.DOScaleX(1f, 0.25f);
        transform.DOScaleY(1f, 0.25f);
    }
}
