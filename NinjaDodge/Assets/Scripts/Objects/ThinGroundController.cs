using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThinGroundController : MonoBehaviour
{
    public static ThinGroundController Instance { get; private set; }
    public ThinGround[] thinGrounds = new ThinGround[4];

    private void Awake()
    {
        Instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F)) thinGrounds[0].Grow();
        if (Input.GetKeyDown(KeyCode.G)) thinGrounds[1].Grow();
        if (Input.GetKeyDown(KeyCode.H)) thinGrounds[2].Grow();
        if (Input.GetKeyDown(KeyCode.J)) thinGrounds[3].Grow();
        if (Input.GetKeyDown(KeyCode.K))
        {
            HideAll();
        }
    }

    public void HideAll()
    {
        foreach (var thinGround in thinGrounds)
        {
            if (thinGround.gameObject.activeSelf == true)
                thinGround.Hide();
        }
    }
}
