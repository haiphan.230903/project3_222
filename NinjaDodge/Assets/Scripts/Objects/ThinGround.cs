using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ThinGround : MonoBehaviour
{
    public Blaster[] blasters;
    public void Grow()
    {
        gameObject.SetActive(true);
        InitBlaster();
        transform.DOMoveY(0, 1);
    }
    public void Hide()
    {
        transform.DOMoveY(-10, 1);      
        StartCoroutine(Cor_Inactive(1));
    }

    public void InitBlaster()
    {
        foreach(var blaster in blasters)
        {
            blaster.Init();
        }
    }

    IEnumerator Cor_Inactive(float sec)
    {
        yield return new WaitForSeconds(sec);
        gameObject.SetActive(false);
    }
}
