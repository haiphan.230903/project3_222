using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Blaster : MonoBehaviour
{
    [SerializeField] GameObject vfx_shotDust;
    [SerializeField] Transform graphicTransform;
    [SerializeField] Bullet bullet;
    [SerializeField] Bird bird, bird2;
    [SerializeField] FireBallRotate fireBall3, fireBall5, fireBallTall3;
    [SerializeField] float minShotDeltaTime, maxShotDeltaTime;
    public float frequenceRate; //0-100

    [SerializeField] bool isBirdBlaster, isFireBallBlaster;

    Coroutine shotCoroutine;
    public void Init()
    {
        StopAllCoroutines();
        shotCoroutine = StartCoroutine(Cor_Shot(Random.Range(minShotDeltaTime, maxShotDeltaTime)));
        transform.localScale = Vector3.one;
    }

    IEnumerator Cor_Shot(float sec)
    {
        yield return new WaitForSeconds(Random.Range(minShotDeltaTime, maxShotDeltaTime));

        if (Random.Range(0, 100) <= frequenceRate)
        {
            if (isBirdBlaster == false && isFireBallBlaster == false)
            {
                graphicTransform.DOScale(1.5f, 0.2f);
                yield return new WaitForSeconds(0.2f);
                graphicTransform.DOScale(1f, 0.2f);
                yield return new WaitForSeconds(0.1f);
            }
            Shot();
        }
        shotCoroutine = StartCoroutine(Cor_Shot(Random.Range(minShotDeltaTime, maxShotDeltaTime)));
    }

    void Shot()
    {
        if (isBirdBlaster == false && isFireBallBlaster == false)
        {
            if (bullet.gameObject.activeSelf == true) return;
            vfx_shotDust.SetActive(true);
            //Bullet bulletScr = Instantiate(this.bulletObj, transform.position, Quaternion.identity).GetComponent<Bullet>();
            bullet.gameObject.SetActive(true);
            bullet.transform.position = transform.position;
            bullet.transform.rotation = Quaternion.Euler(0, 0, 0);
            bullet.SetDir(transform.rotation.eulerAngles.y * Mathf.Deg2Rad);
        }
        else
        {
            if (isBirdBlaster)
            {
                if (Random.Range(0, 100) < 90)
                {
                    if (bird.gameObject.activeSelf == true) return;
                    bird.gameObject.SetActive(true);
                    bird.transform.position = transform.position;
                    bird.transform.rotation = Quaternion.Euler(0, 0, 0);
                    bird.SetDir(transform.rotation.eulerAngles.y * Mathf.Deg2Rad);
                }
                else
                {
                    if (bird2.gameObject.activeSelf == true) return;
                    bird2.gameObject.SetActive(true);
                    bird2.transform.position = transform.position + Vector3.up * 2;
                    bird2.transform.rotation = Quaternion.Euler(0, 0, 0);
                    bird2.SetDir(transform.rotation.eulerAngles.y * Mathf.Deg2Rad);
                }
            }
            if (isFireBallBlaster)
            {
                if (Random.Range(0, 100) < 60)
                {
                    if (fireBall3.gameObject.activeSelf == true) return;
                    fireBall3.gameObject.SetActive(true);
                    fireBall3.transform.position = transform.position;
                    fireBall3.transform.rotation = Quaternion.Euler(0, 0, 0);
                    fireBall3.SetDir(transform.rotation.eulerAngles.y * Mathf.Deg2Rad, (Random.Range(1,100)%2*2-1)*90);
                }
                else if (Random.Range(0, 100) < 80)
                {
                    if (fireBall5.gameObject.activeSelf == true) return;
                    fireBall5.gameObject.SetActive(true);
                    fireBall5.transform.position = transform.position;
                    fireBall5.transform.rotation = Quaternion.Euler(0, 0, 0);
                    fireBall5.SetDir(transform.rotation.eulerAngles.y * Mathf.Deg2Rad, (Random.Range(1, 100) % 2 * 2 - 1) * 90);
                }
                else
                {
                    if (fireBallTall3.gameObject.activeSelf == true) return;
                    fireBallTall3.gameObject.SetActive(true);
                    fireBallTall3.transform.position = transform.position;
                    fireBallTall3.transform.rotation = Quaternion.Euler(0, 0, 0);
                    fireBallTall3.SetDir(transform.rotation.eulerAngles.y * Mathf.Deg2Rad, (Random.Range(1, 100) % 2 * 2 - 1) * 90);
                }
            }
        }
    }
}
