using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBallRotate : MonoBehaviour
{
    [SerializeField] Transform containTrans;
    Vector3 dir;
    public float speed, rotateSpeed;

    public void SetDir(float blasterAngle, float rotateSpeed) //basterAngle in rad
    {
        this.rotateSpeed = rotateSpeed;
        dir = new Vector3(Mathf.Sin(blasterAngle), 0f, Mathf.Cos(blasterAngle)).normalized;
        StartCoroutine(Cor_Inactive(10));
        gameObject.SetActive(true);
    }
    // Update is called once per frame
    void Update()
    {
        Move();
    }

    void Move()
    {
        transform.Translate(dir * speed * Time.deltaTime);
        containTrans.Rotate(new Vector3(0, rotateSpeed * Time.deltaTime, 0));
    }

    IEnumerator Cor_Inactive(float sec)
    {
        yield return new WaitForSeconds(sec);
        gameObject.SetActive(false);
        StopAllCoroutines();
    }

    private void OnDisable()
    {
        gameObject.SetActive(false);
    }
}
