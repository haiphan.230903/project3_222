using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bird : MonoBehaviour
{
    [SerializeField] Rigidbody rb;
    [SerializeField] float speed;
    Vector3 dir;

    [SerializeField] GameObject vfx_explose;
    public void SetDir(float blasterAngle) //basterAngle in rad
    {
        rb.velocity = Vector3.zero;
        dir = new Vector3(Mathf.Sin(blasterAngle), 0, Mathf.Cos(blasterAngle)).normalized;
        transform.Translate(dir / 2);
        StartCoroutine(Cor_Inactive(6));

        gameObject.SetActive(true);
    }
    // Update is called once per frame
    void Update()
    {
        Move();
    }

    void Move()
    {
        transform.Translate(dir * speed * Time.deltaTime);
    }

    IEnumerator Cor_Inactive(float sec)
    {
        yield return new WaitForSeconds(sec);
        gameObject.SetActive(false);
        StopAllCoroutines();
    }

    private void OnDisable()
    {
        gameObject.SetActive(false);
    }
}
