using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.Rendering;

public class ButtonAnim : MonoBehaviour
{
    [SerializeField] RectTransform buttonRect, cloudRect, treeRect;
    Coroutine mouseEnterCor;
    public void MouseEnter()
    {
        buttonRect.DOScale(1.2f, 0.3f);
        if (cloudRect != null || treeRect != null)
            mouseEnterCor = StartCoroutine(Cor_MouseEnterAnim());
    }
    public void MouseExit()
    {
        buttonRect.DOScale(1f, 0.3f);
        if (mouseEnterCor != null)
            StopCoroutine(mouseEnterCor);
        DOTween.Kill(cloudRect);
        DOTween.Kill(treeRect);
        if (cloudRect != null)
            cloudRect.DOAnchorPosX(0, 0.5f);
        if (treeRect != null)
            treeRect.DOAnchorPosX(0, 0.5f);
    }

    public void MouseDown()
    {
        buttonRect.DOScale(0.9f, 0.3f);
    }
    public void MouseClick()
    {
        buttonRect.DOShakeScale(1, 0.4f);
    }

    IEnumerator Cor_MouseEnterAnim()
    {
        cloudRect.DOAnchorPosX(150, 3);
        if (treeRect != null)
            treeRect.DOAnchorPosX(-130, 3);
        yield return new WaitForSeconds(3);
        cloudRect.DOAnchorPosX(-150, 3);
        if (treeRect != null)
            treeRect.DOAnchorPosX(130, 3);
        yield return new WaitForSeconds(3);
        mouseEnterCor = StartCoroutine(Cor_MouseEnterAnim());
    }
}
