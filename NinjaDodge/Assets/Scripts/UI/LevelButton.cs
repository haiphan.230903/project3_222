using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelButton : MonoBehaviour
{
    [SerializeField] Image[] starImage = new Image[3];
    [SerializeField] Sprite starActive, starInactive;
    [SerializeField] int id;
    public int star;

    public void SetStar()
    {
        if (LevelController.Instance.starLevels[id] <= star) return;

        star = LevelController.Instance.starLevels[id];
        for (int i =0; i<3; i++)
        {
            if (i < star)
            {
                starImage[i].sprite = starActive;
            }
            else
            {
                starImage[i].sprite = starInactive;
            }
        }
    }
}
